# -*- coding: utf-8 -*-
"""
Created on Tue Mar  6 19:18:39 2018

@author: 02012018
"""

from datetime import datetime, timezone

from zipline.api import record, symbol, order
from zipline import run_algorithm

import pandas as pd

def initialize(context):
    pass

def handle_data(context, data):
    order(symbol('AAPL'), 10)
    record(APPL=data.current(symbol('AAPL'), 'price'))
    
start = datetime (2016, 3, 1, tzinfo=timezone.utc)
end = datetime(2016, 4, 1, tzinfo=timezone.utc)
start = pd.Timestamp(start, tz='UTC', offset='C')
end = pd.Timestamp(end, tz='UTC', offset='C')

results = run_algorithm(
        start = start,
        end = end,
        initialize = initialize,
        handle_data = handle_data,
        capital_base = 10000,
        data_frequency = 'daily')

print('hello')

# LU 15
def initialize (context):
    #We select the securities that we want
    context.security =symbol ("TSLA")
    #schedule the function that will run every day at market open
    schedule_function(func=action_function,
                       date_rule=date_rules.every_day(),
                       time_rule=time_rules.market_open(hours=0, minutes=1))

def action_function(context, data):
    #Prices for the last 5 days for our security
    price_history = data.history(context.security,
                                 fields='price',
                                 bar_count=10,
                                 frequency='1d'
                                 )
    average_price = price_history.mean()
    current_price = data.current(assets=context.security, fields='price')
    
    if data.can_trade(context.security):
        #This will buy me 1000$ shares of Tesla
        if current_price > average_price * 1.02:
            #We allocate our entire portfolio to Tesla
            order_target_percent(context.security, 1)
            log.info('Buying + {}'.format(context.security))
        else:
            #This will set my positions to zero
            order_target_percent(context.security, 0)
            log.info('Setting my position to 0 on {}'.format(context.security.symbol))