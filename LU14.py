# -*- coding: utf-8 -*-
"""
Created on Tue Apr 10 19:47:38 2018

@author: 02012018
"""

#use of parentheses
def function1(a):
    print(a+2)
    
a = function1
b = function1(2)

a(1)

'''
if we assign a function to a variable like a=function1 then a will be a new function which wil do everything
like function1 does, if we assign a function to a variable like a=function(), we will need to give a value to
that function1, if it works without parameters function1 is enough, if it needs parameters we will need to
give it a parameter in order to the variable have a variable, cause with parentheses it will only be a variable
'''

def func_caller(func):
    func()

def law_and_order():
    print('law and order')
    
func_caller(law_and_order)

k = 1

#Exercise 1

def func_caller(func):
    func()
    return None #non mandatory

m = func_caller

#Exercise 2
    
def func_caller(func1, func2):
    func1()
    func2()
    return None

#Exercise 3
    
def func_caller(a, func):
    if a>10:
        func()
        
#Exercise 4

def func_caller(func1, func2):
    if func1():
        func2()
    
#Exercise 5

def func_caller(func):
    return func()

#Exercise 6
    
def func_caller(func1, func2):
    if func1():
        return func2()
    

        