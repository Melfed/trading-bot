# LU 9 - Reading Docs
## Exercise 1
Zipline is a pythonic algorithmic trading library. It is an event-driven system that supports both backtesting and live-trading.
Zipline is currently used in production as the backtesting and live-trading engine powering Quantopian - a free, community-centered, hosted platform
for building and executing trading strategies.

Zipline - Engine, develop your strategy offline and then port to Quantopian for paper trading (live but for free - testing) and live trading,
using the get_environment method.

Quantopian - Provides you with eberything you need to write a high-quality algorithmic trading strategy. Ise a bariaty of data sources,
test your strategy over historical data, test it going forward with live data. Crowd-sourced quantitative investment firm. Development firm.

## Exercise 2
Slippage - Realistic impact of your orders on the price.

set_slippage sets this slippage. Can be:
1. FixedBasisPointsSlippage(basis_points = 5, volume_limit = 0.1), where basis_points sets the new price after the order: NewPrice = Price * 1.005
and volume_limit is the maximum shares per transaction = total number of shares * volume_limit
2. FixedSlippage(spread), half of the spread is added to the price on buys, and taken from the price on sells
3. VolumeSharesSlippage(volume_limit, price_impact)
4. Custom Slippage

## Exercise 3
It is buying 100 shares of both Apple and Microsoft in the first day in 2008, and will hold them until the end (2013) and not buy or sell anymore as
context.has_ordered is True after the first buy.

## Exercise 4



changed